USE bibliothque;

-- Création d'une vue
CREATE VIEW v_livre_genre AS
SELECT livres.id,titre,annee, genres.nom AS categorie,
 concat(prenom,' ',auteurs.nom) AS auteurs
FROM livres
INNER JOIN genres ON livres.genre =genres.id 
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre 
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur;

-- Avec SHOW tables la vue est considérée comme une table 
SHOW tables;

-- Utilisation de la vue
SELECT * FROM v_livre_genre ;

SELECT titre, auteurs, annee,categorie  FROM v_livre_genre 
WHERE annee BETWEEN 1970 AND 2010;

-- Ajouter un livre dans la bibliotheque
INSERT INTO livres(titre, annee,genre)
VALUES('Gomora',2006,1 );

INSERT INTO livre2auteur (id_auteur,id_livre)
VALUES(39,143);

-- la vue est mise à jour automatiqument
SELECT titre, auteurs, annee,categorie  FROM v_livre_genre 
WHERE annee BETWEEN 1970 AND 2010;

-- Supprimer une vue 
DROP VIEW v_livre_genre;

-- recréation d'une vue
ALTER VIEW v_livre_genre AS 
SELECT livres.id AS id_livre,titre,annee, genre , genres.nom AS categorie
FROM livres
INNER JOIN genres ON livres.genre =genres.id;

SELECT * FROM v_livre_genre ;

-- Insertion de donnée via la vue

-- On ne peut pas ajouter des données via une vue, si elles sont sur plusieurs tables
-- ->Erreur
-- INSERT INTO v_livre_genre(id_livre,titre,annee,id_genre,categorie)
-- VALUES (144,'les misérable',1862,7,'Drame');

-- On peut ajouter des données via une vue, mais uniquement sur une seule table
-- ici livres
INSERT INTO v_livre_genre(titre,annee,genre)
VALUES ('les misérable',1862,7);

-- ici genres
DELETE FROM genres WHERE id=16;
INSERT INTO v_livre_genre(categorie) VALUES ('humour');

INSERT INTO v_livre_genre(titre,annee,genre)
VALUES ('les 100 meilleurs blagues belge',1987,19);

-- idem pour la mise à jour des données
UPDATE v_livre_genre SET titre='les 150 meilleurs blagues belge'  WHERE id_livre=145;

-- Exercice vue
USE hr;
-- Créer une vue qui a pour colonne:
-- - l'id de l'employé
-- - la première lettre de son prenom + '. ' + nom
-- - l'intitulé de son poste
-- - son salaire
-- - le nom de son département
-- - le nom de son pays
-- - la date d'engagement

CREATE VIEW v_employees AS 
SELECT employees.employee_id,concat(LEFT(first_name,1),'. ',last_name) AS nom, jobs.job_title,employees.salary,
departments.department_name, countries.country_name, employees.hire_date 
FROM employees 
INNER JOIN jobs ON jobs.job_id= employees.job_id
INNER JOIN departments ON departments.department_id= employees.department_id
INNER JOIN locations ON locations.location_id= departments.location_id
INNER JOIN countries ON countries.country_id=locations.country_id; 

-- En utilisant la vue :

-- - afficher les employés qui sont depuis plus de 30 ans dans l'entreprise
SELECT nom,hire_date  FROM v_employees WHERE YEAR(Current_date())-YEAR(hire_date)>=30;

-- - augmenter les salaires de 10% pour les employés qui ont un salaire < à 3500
UPDATE v_employees SET salary=salary * 1.10 WHERE salary<3000;
