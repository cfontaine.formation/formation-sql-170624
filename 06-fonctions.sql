USE bibliotheque;

-- Syntaxe d'une fonction
-- nom_fonction(param1,param2 ...)

-- Fonctions arithmétiques

-- CEIL(1.4) -> 2, 
-- FLOOR(1.6) -> 1
-- ROUND(1.4628476,3) -> 1.463
-- TRUNCATE(1.4628476,3) -> 1,462
-- RAND() -> nombre aléatoire entre 0 et 1

SELECT CEIL(1.4), FLOOR(1.4), TRUNCATE(1.4628476,3), ROUND(1.4628476,3), RAND();

-- On peut utiliser rand() pour sélectionner aléatoirement 5 livres 
SELECT titre FROM Livres ORDER BY rand() LIMIT 5; 

-- Fonction Chaine de caractère
-- - Taille de la chaine de caractère nom en octets
-- - Nombre de caractère de la chaine de caractère nom 
SELECT prenom, LENGTH(prenom), char_length(prenom) FROM auteurs; 

-- On ne sélectionne que les lignes de la table auteurs dont le prenom fait moins de 5 caratères
SELECT prenom FROM auteurs WHERE char_length(prenom)<5; 

-- concat -> Concaténation de chaînes
SELECT concat(prenom, ' ', nom) FROM  auteurs; 

-- concat_ws -> Concaténation de chaînes avec un séparateur (->1er paramètre)
SELECT concat_ws(' ',prenom, nom, naissance) FROM  auteurs;

-- space(20) -> retourne une chaîne contenant 20 espaces
SELECT concat('|',SPACE(30),'|'),ascii('a');

-- insert: Insertion d'une chaîne à une position pos et pour num caractères
-- Insertion d'une chaîne ---- au 3 caractères => Ja---mes -> Ja---mes
SELECT prenom, INSERT(prenom,3,0,'----') FROM auteurs ;
-- Insertion d'une chaîne ---- au 3 caractères et pour remplacer 2 caractères
-- => Ja---mes -> Ja---s
SELECT prenom, INSERT(prenom,3,2,'----') FROM auteurs ;

-- remplace: Remplace toutes les occurrences d'une sous-chaîne par une nouvelle chaîne
-- remplace dans le nom er par ---
-- Hebert -> H---b---t
SELECT prenom, REPLACE(prenom,'er','***')FROM auteurs ;

-- repeat -> répéte le nom 3 fois
-- reverse -> Inverse les caractères du nom (Ellroy -> yorllE)
SELECT nom,repeat(nom,3),reverse(nom) FROM auteurs;

-- left(prenom,2) -> Extrait 2 caractère en partant de la gauche du prénom
-- right(prenom,2) -> Extrait 2 caractère en partant de la droite du prénom
-- substr(prenom,2,3) -> Extraction d'une sous chaîne du prénom à partir du 2ème caractères et pour 3 caractères
SELECT prenom, LEFT(prenom,2),RIGHT (prenom,3),substr(prenom,2,3) FROM auteurs ;

-- Renvoie la position de la première occurrence de la chaîne 'er', dans le prenom
SELECT POSITION('er' IN prenom) FROM auteurs; 

-- FIND_IN_SET Renvoie la position de la chaîne aze dans la chaîne 'ert,qez,sdvqh,aze,rty'
-- contenant une liste de sous-chaîne séparé par une virgule -> 4
SELECT FIND_IN_SET('aze','ert,qez,sdvqh,aze,rty');

-- FIELD Renvoie la position de 'ert' dans la liste de valeur 'gjhghj','ggujh','ert','aze','rty'
SELECT FIELD('ert','gjhghj','ggujh','ert','aze','rty'); -- -> 3

-- LTRIM -> Retire les caractère blanc à gauche de la chaîne
-- RTRIM -> Retire les caractère blanc à droite de la chaîne
-- TRIM ->Retire les caractère blanc à droite et à gauche
SELECT LTRIM('      hello world     '),
       RTRIM('      hello world      '),
       TRIM('      hello world      ');

-- lower(prenom) -> Conversion en minuscule du prénom
-- upper(prenom) -> Conversion en majuscule du prénom
SELECT LOWER(prenom),UPPER(nom) FROM auteurs;

-- strcmp -> Compare 2 chaînes : 
-- 'bonjour' < 'hello' → -1
-- 'bonjour' > 'hello' → 1
-- 'bonjour' = 'bonjour' → 0 
SELECT STRCMP('bonjour','hello'),STRCMP('hello','bonjour'),STRCMP('bonjour','bonjour');

-- 1980000 -> 4 chiffres après la virgule 1,980,000.00
SELECT FORMAT(annee,2) FROM livres;

-- Fonction Temporelle
-- CURRENT_DATE() -> Date courante
-- CURRENT_TIME() -> Heure courante
-- CURRENT_TIMESTAMP() ou NOW() -> Date et heure courante
SELECT current_date(), current_time(), current_timestamp(),now();

-- DATE -> Extrait la date de la Date et de l'heure courante -> 2023-02-14
-- Day -> Extrait le jour de la Date et de l'heure courante -> 14
-- LAST_DAY -> Le dernier jour du mois (30,31,29,28)
SELECT DATE(now()),DAY(now()),LAST_DAY(now());

-- MONTH -> Extrait le mois -> 10
-- YEAR -> Extrait l'année -> 2023
-- dayofmonth -> Extrait le jour -> 20
-- quarter -> retourne le trimestre d'une date -> 3
-- WEEK -> Numéro de semaine d'une date (0 à 53)
-- dayofyear -> jour de l'année (1 à 366)
SELECT nom,naissance,MONTH(naissance),YEAR(naissance),
       dayofmonth(naissance),quarter(naissance),week(naissance),
       dayofyear(naissance)
FROM auteurs;

-- dayofweek -> Le numéro du jour de la semaine d'une date (1→ dimanche … 7→ samedi)
-- weekday -> Numéro de jour de la semaine (0 → lundi … 6 → dimanche)
SELECT dayofweek(naissance),weekday(naissance) FROM auteurs;

-- Requete pour obtenir l'age d'un livre 
SELECT YEAR(now())-annee FROM livres;

-- Nombre de jour entre 2 dates
SELECT datediff('2024-08-01',current_date()); -- 140

-- trimestre , jour de l'année et numéro de semaine de la naissance de l'auteur
SELECT prenom,nom,quarter(naissance),dayofyear(naissance),week(naissance)
FROM auteurs

-- Age des auteurs lorsqu'ils sont décédés
SELECT prenom,nom, ROUND(datediff(deces,naissance)/365) FROM auteurs ;

-- ADDDATE -> Ajoute un intervalle à une date
-- SUBDATE -> Soustrait un intervalle à une date
SELECT DATE_ADD(current_date(), INTERVAL 2 DAY),DATE_SUB(current_date(), INTERVAL 1 YEAR);

-- DATE_FORMAT -> Formater une date
-- https://mariadb.com/kb/en/date_format/
SELECT date_format(naissance,'%m %Y (%j) %a') FROM auteurs;

-- STR_TO_DATE -> Convertir une chaîne en date suivant un format
SELECT str_to_date('10/09/2018','%d/%m/%Y');

-- L'heure
-- HOUR -> extraire les heures
-- MINUTE -> extraire les minutes
-- SECOND -> extraire les secondes
-- microsecond -> extraire les microsecondes
SELECT HOUR(current_time()),MINUTE(current_time()),SECOND(current_time()),microsecond(current_time()) ;

-- timediff -> Différence entre 2 heures
-- subtime -> Soustrait un intervalle à une heure
-- addtime -> Ajoute un intervalle à une heure
SELECT timediff('12:30:00',current_time()),subtime(current_time(),'05:00:00'),addtime(current_time(),'05:00:00');

-- time_to_sec -> Conversion d’une heure en seconde
SELECT time_to_sec(current_time());

-- TIME_FORMAT -> Formater une heure suivant un format
SELECT TIME_FORMAT(current_time(),'%l:%i:%s %p');

-- Fonction d'agregation
SELECT count(id), min(naissance), max(naissance) FROM auteurs;

-- Autre fonction
-- COALESCE -> Retourne la première valeur non-null de la liste
SELECT prenom, nom, COALESCE(deces,'vivant') FROM auteurs;

-- NULLIF -> Retourne NULL si les 2 paramètres sont égaux sinon retourne le premier paramètre
SELECT annee,NULLIF(annee,1954) FROM livres;

-- current_user -> utilisateur courant
-- DATABASE () -> base de donnée courante
-- VERSION() -> version de la sgbd
-- last_insert_id() -> L’id AUTO_INCREMENT de la dernière ligne qui a été insérée ou modifiée
SELECT current_user(),DATABASE(),VERSION(),last_insert_id();

-- Fonction if
-- On affiche pour les auteurs vivants leur age et pour les auteurs morts leur age au moment de leur mort
SELECT prenom,nom, 
IF( deces IS NOT NULL,
   ROUND(datediff(deces,naissance)/365) ,
   ROUND(datediff(current_date(),naissance)/365))
FROM auteurs ;

-- Fonction CASE
-- CASE utilisant des conditions
SELECT annee, titre,
CASE
	WHEN annee>2000 THEN '21 eme siecle'
	WHEN annee BETWEEN 1901 AND 2000 THEN '20 eme siecle'
	WHEN annee BETWEEN 1801 AND 1900 THEN '19 eme siecle'
	ELSE 'Livre ancien'
END 
FROM livres;
END

-- CASE utilisant des valeurs
SELECT titre, genre,
CASE(genre)
	WHEN 1 THEN 'Noir'
	WHEN 6 THEN 'Vert'
	WHEN 7 THEN 'Rouge'
	ELSE 'Bleu'
END
FROM livres;

SELECT titre,genre,
CASE genres.nom 
	WHEN 'Policier' THEN 'Noir'
	WHEN 'Science-fiction' THEN 'Vert'
	WHEN 'Drame' THEN 'Rouge'
	ELSE 'Bleu'
END
FROM livres
INNER JOIN genres ON genres.id=livres.genre;
-- Exercice Fonctions
USE world;

-- Nombre de pays présents dans la table Country
SELECT count(Code) FROM country;

-- Écrire une requête pour générer un code qui a pour forme :
-- les 3 première lettres de la ville concaténé avec la chaine '0000'et le nombre de caractère de la ville et séparé par -
SELECT name,concat(LEFT(name,3),'-0000-',char_length(name)) FROM city;

SELECT CONCAT_WS('-', LEFT(name,3) , '0000' , char_length(name)) FROM city; 

-- Afficher le nombre de pays en europe
SELECT count(Code) FROM country WHERE continent ='Europe';

USE elevage;

-- Afficher le nombre de race dans la table Race
SELECT count(id)  FROM race;

-- Afficher le nom des animaux nés en 2006
SELECT nom,date_naissance FROM animal
WHERE YEAR(date_naissance)=2006; 

-- Afficher le nom de tous les animaux nés dans les huit premières semaines d'une année.
SELECT nom,date_naissance FROM animal WHERE WEEK(date_naissance)<=8;

-- Afficher les chats dont la deuxième lettre du nom est un a
SELECT animal.nom FROM animal 
INNER JOIN espece ON animal.espece_id =espece.id 
WHERE espece.nom_courant = 'chat' AND animal.nom LIKE '_a%';
-- ou
SELECT  animal.nom FROM animal 
INNER JOIN espece ON animal.espece_id =espece.id 
WHERE espece.nom_courant = 'chat' AND POSITION('a' IN animal.nom)=2;

-- Afficher les chiens dont le nom a un nombre pair de lettres
SELECT  animal.nom, char_length(animal.nom),espece.nom_courant  FROM animal 
INNER JOIN espece ON animal.espece_id =espece.id AND char_length(animal.nom) MOD 2 = 0
WHERE espece.nom_courant = 'chien';

-- Afficher le nombre de chiens dont on connait le père
SELECT  count(animal.id) FROM animal 
INNER JOIN espece ON animal.espece_id =espece.id 
WHERE espece.nom_courant = 'chien' AND animal.pere_id IS NOT NULL