USE bibliotheque;

-- Jointure interne

-- Relation 1,N
-- On va joindre la table livres et la table genres en utilisant l'égalité entre
-- La clé primaire de genres -> id et la clé étrangère de livre -> genre
SELECT titre,annee,genres.nom AS genre FROM livres
INNER JOIN genres ON livres.genre = genres.id;

-- Jointure entre le table auteurs et la table pays
-- Afficher le prénom et le nom de l'auteur et le nom du pays
SELECT prenom, auteurs.nom,pays.nom FROM auteurs 
INNER JOIN pays ON auteurs.nation = pays.id ;

-- Jointure interne avec un SELECT et un WHERE 
SELECT prenom, auteurs.nom,pays.nom,nation,pays.id FROM auteurs,pays
WHERE auteurs.nation=pays.id;

-- Afficher le titre, l'année de sortie et le genre du livre qui sont sortie entre 1980 et 1990
SELECT titre,annee,genres.nom AS genre FROM livres
INNER JOIN genres ON livres.genre = genres.id
WHERE annee BETWEEN 1980 AND 1990 ORDER BY annee;

-- Relation n-n
-- On va joindre : la table livres et la table de jointure livre2auteur
-- On va joindre le resultat de la jointure précédente avec la table auteurs
SELECT titre,prenom,nom FROM livres
INNER JOIN livre2auteur ON livres.id= livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur;

SELECT titre,prenom,nom FROM livres
INNER JOIN livre2auteur ON livres.id= livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur
WHERE prenom='Pierre';

-- afficher les 5 auteurs français ou italien qui ont un livre le plus récemment 
SELECT titre,annee,prenom,auteurs.nom,pays.nom FROM livres
INNER JOIN livre2auteur ON livres.id= livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur
INNER JOIN pays ON auteurs.nation = pays.id
WHERE pays.nom IN ('France','italie') AND annee>1960 ORDER BY annee DESC LIMIT 5;

-- Exercice Jointure interne
USE world;
-- Afficher les nom de pays et le nom de ses villes classer par nom de pays puis par nom de ville en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name , city.name FROM city
INNER JOIN country ON city.country_code =country.code 
ORDER BY country.name , city.name ;

-- Afficher: les noms de pays, la langue et le pourcentage classé par pays et par pourcentage décroissant
SELECT name, country_language.language, percentage FROM country
INNER JOIN country_language ON country.code= country_language.country_code
ORDER BY country.name , percentage DESC;

-- Afficher chaque nom de pays et le nom de sa capitale
SELECT country.name, city.name FROM country
INNER JOIN city ON city.id= country.capital; 

-- Produit cartésien -> CROSS JOIN
-- On obtient toutes les combinaisons possible entre les 2 tables
-- Le nombre d'enregistrements obtenu est égal au nombre de lignes de la première table multiplié par le nombre de lignes de la deuxième table.
USE exemple;

CREATE TABLE plats
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO plats(nom) VALUES
('Ceréale'),
('Pain'),
('Oeuf sur le plat');

CREATE TABLE boissons
(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO boissons(nom) VALUES
('thé'),
('café'),
('jus d''orange');

-- On obtient toutes les combinaisons possible entre les boissons et les plats
SELECT plats.nom AS plat,boissons.nom AS boisson FROM plats 
CROSS JOIN boissons;

USE bibliotheque ;

-- Jointure externe: LEFT JOIN ou RIGTH JOIN
SELECT genres.nom, livres.titre ,annee FROM genres
LEFT JOIN livres ON genres.id =livres.genre;

-- équivalent avec un right join
SELECT genres.nom ,titre FROM livres
RIGHT JOIN genres ON genres.id =livres.genre;

-- Obtenir tous les genres qui ne sont pas représentés dans la bibliotheque
SELECT genres.nom FROM genres
LEFT JOIN livres ON genres.id =livres.genre 
WHERE livres.id IS NULL;

-- Obtenir tous les auteurs dont la nationalité n'a pas été renseigné
SELECT prenom, auteurs.nom, pays.nom AS pays FROM auteurs
LEFT JOIN pays ON nation=pays.id
WHERE pays.id IS NULL;

-- Obtenir tous les pays qui n'ont pas d'auteur
SELECT prenom, auteurs.nom, pays.nom AS pays FROM auteurs
RIGHT JOIN pays ON nation=pays.id
WHERE auteurs.id IS NULL;

-- FULL JOIN
-- SELECT a.prenom,a.nom,p.nom AS nationalite  FROM auteurs a
-- FULL JOIN pays AS p ON a.nation = p.id ;

-- FULL JOIN n'est pas encore supporté par MySql/MariaDb
-- Mais on peut le réaliser avec cette requète
SELECT prenom, auteurs.nom, pays.nom AS pays FROM auteurs
LEFT JOIN pays ON nation=pays.id
UNION 
SELECT prenom, auteurs.nom, pays.nom AS pays FROM auteurs
RIGHT JOIN pays ON nation=pays.id;

-- SELECT prenom, auteurs.nom, pays.nom AS pays FROM auteurs
-- FULL JOIN pays ON nation=pays.id
-- WHERE pays.id IS NULL OR auteurs.id IS NULL;

SELECT prenom, auteurs.nom, pays.nom AS pays FROM auteurs
LEFT JOIN pays ON nation=pays.id
WHERE pays.id IS NULL
UNION 
SELECT prenom, auteurs.nom, pays.nom AS pays FROM auteurs
RIGHT JOIN pays ON nation=pays.id
WHERE auteurs.id IS NULL;

-- Jointure naturelle -> uniquement mysql et mariadb
-- la clé primaire et la clé étrangère doivent avoir le même nom

-- Changer le nom de la clé primaire  id -> genre
ALTER TABLE livres DROP CONSTRAINT fk_genres; 
ALTER TABLE genres CHANGE id genre INT; 

ALTER TABLE livres ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES genres(genre);

-- Jointure naturelle
SELECT titre, annee,nom FROM livres
NATURAL JOIN genres;

-- Changer le nom de la clé primaire genre -> id
ALTER TABLE livres DROP CONSTRAINT fk_genres; 
ALTER TABLE genres CHANGE genre id INT; 

ALTER TABLE livres ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES genres(id);

-- Auto jointure
-- Une table que l'on joindre avec elle même

-- Une auto joiture est souvent utilisé pour représenter
-- une hierarchie en SQL (relation parent->enfant)
USE exemple;
CREATE TABLE salaries(
	id int PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	manager int ,
	
	CONSTRAINT fk_manager
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries(prenom,nom,manager) VALUES  ('John','Doe',NULL);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jane','Doe',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Jo','Dalton',1);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Alan','Smithee',2);
INSERT INTO salaries(prenom,nom,manager) VALUES  ('Yves','Roulo',2);

SELECT * FROM salaries ;

SELECT employes.prenom AS prenom , employes.nom AS nom, managers.prenom AS prenom_manager, managers.nom AS nom_manager  FROM salaries AS employes
LEFT JOIN salaries AS managers 
ON employes.manager=managers.id

-- Exercice: Jointure interne, externe et auto jointure
USE world;

-- Afficher: les noms de  pays, la  langue et le  pourcentage 
-- classé par pays et par  pourcentage décroissant. 
-- mais on ne veut obtenir que les langues officielles
SELECT name, LANGUAGE, percentage FROM country 
INNER JOIN country_language ON country_language.country_code = country.code 
WHERE country_language.is_official = 'T'
ORDER BY country.name, percentage DESC;

-- Afficher le nom des pays sans ville
SELECT country .name, city.name FROM country  
LEFT JOIN city ON city.country_code=country.code
WHERE city.name IS NULL;

-- Afficher tous les pays qui parlent français
SELECT country.name FROM country 
INNER JOIN country_language ON country.code= country_language.country_code 
WHERE country_language.LANGUAGE='French';

USE hr;

-- On veut obtenir tous les pays pour lequel il n'y a pas de location
SELECT countries.country_name FROM countries
LEFT JOIN locations ON countries.country_id =locations.country_id
WHERE locations.country_id IS NULL;

-- On veut obtenir le nom des employés qui travaillent au département finance
SELECT employees.first_name, employees.last_name, department_name
FROM employees
INNER JOIN departments ON departments.department_id =employees.department_id
WHERE department_name='finance' ;

-- On veut obtenir le nom des régions pour lequel il n'y a pas de location
SELECT regions.region_name  FROM countries
INNER JOIN locations ON countries.country_id =locations.country_id
RIGHT JOIN  regions ON countries.region_id=regions.region_id
WHERE locations.country_id IS NULL;

-- auto jointure
USE elevage;

-- Afficher  la liste des enfants de Bouli
SELECT pere.nom AS nom_parent,enfant.nom  AS nom_enfant 
FROM animal AS pere 
LEFT JOIN animal AS enfant 
ON pere.id= enfant.pere_id ;
WHERE pere.nom='Bouli';

-- Afficher la liste des chats dont on connaît les parents, ainsi que le nom de ces parents
SELECT enfant.nom  AS enfant,pere.nom AS pere, mere.nom AS mere
FROM animal AS pere 
LEFT JOIN animal AS enfant  ON pere.id= enfant.pere_id 
LEFT JOIN animal AS mere  ON mere.id= enfant.mere_id 
INNER JOIN espece ON enfant.espece_id = espece.id
WHERE espece.nom_courant = 'Chat';

