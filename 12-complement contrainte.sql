USE exemple;

DROP TABLE personnes;

-- CHECK -> permet de limiter la plage de valeur
CREATE TABLE personnes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	-- le nom doit comporter au moins 2 caractères
	-- et le premier caractère doit être un lettre entre A et Z
	--    les autres caractères doivent être entre A et Z , entre  a et z, un espace ou un chiffre entre 0 et 9
	nom VARCHAR(50) NOT NULL CHECK(CHAR_LENGTH(nom)>=2 AND nom RLIKE '^[A-Z][a-z A-Z0-9]*'),
	-- l'age doit être compris entre 0 et 130
	age INT NOT NULL CHECK(age BETWEEN 0 AND 130)
);

INSERT INTO personnes(prenom,nom,age) VALUE ('John','Doe',38);
-- Erreur --> le nom ne fait qu'un seul caractère
INSERT INTO personnes(prenom,nom,age) VALUE ('John','D',48);
-- Erreur --> l'age est inférieur à 0
INSERT INTO personnes(prenom,nom,age) VALUE ('John','Doe',-28);
-- Erreur --> l'age est supérieur à 130
INSERT INTO personnes(prenom,nom,age) VALUE ('John','Doe',285);
-- Erreur --> le nom commence par une minuscule
INSERT INTO personnes(prenom,nom,age) VALUE ('John','doe',28);
-- Erreur --> le nom commence par un chiffre
INSERT INTO personnes(prenom,nom,age) VALUE ('John','123xyz',28);

DROP TABLE personnes;

-- Containte CHECK sur plusieurs colonnes
CREATE TABLE personnes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom VARCHAR(50) NOT NULL CHECK(CHAR_LENGTH(nom)>=2 AND nom RLIKE '^[a-z A-Z]+'),
	age INT NOT NULL ,
	pays VARCHAR(50) NOT NULL,
	-- les contraintes sont sur les colonnes age et oays
	CONSTRAINT chk_personne 
	CHECK(
			(age>=13 AND pays= 'france') OR -- 1
			(age>=21 AND pays='usa') OR -- 2
			(age>=17 AND pays<> 'france' AND pays<>'usa') -- 3
		)
);

INSERT INTO personnes (prenom,nom,age,pays) VALUES('Jane','Doe',24,'Espagne');
INSERT INTO personnes (prenom,nom,age,pays) VALUES('John','Doe',14,'France');
-- Erreur --> la contrainte 3 n'est pas respectée age<17
INSERT INTO personnes (prenom,nom,age,pays) VALUES('John','Doe',14,'Espagne');
-- Erreur --> la contrainte 2 n'est pas respectée age<21
INSERT INTO personnes (prenom,nom,age,pays) VALUES('Jo','Dalton',19,'USA');
-- Erreur --> la contrainte 1 n'est pas respectée age<12
INSERT INTO personnes (prenom,nom,age,pays) VALUES('Jo','Dalton',12,'France');

DROP TABLE personnes;

-- Ajout d'une contrainte CHECK après la création de la table avec ALTER TABLE
CREATE TABLE personnes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom VARCHAR(50) NOT NULL CHECK(CHAR_LENGTH(nom)>=2 AND nom RLIKE '^[A-Z][a-z A-Z0-9]*'),
	age INT NOT NULL 
);
-- Ajout d'une colonne pays dans la table personnes
ALTER TABLE personnes ADD pays VARCHAR(50) NOT NULL;

ALTER TABLE personnes ADD CONSTRAINT chk_age_pays
CHECK((age>=18 AND pays='france')
		OR (age>=21 AND pays='usa')
		OR (age>=17 AND pays!='france' AND pays!='usa')
);

INSERT INTO personnes (prenom,nom,age,pays) VALUES('Jane','Doe',17,'Espagne');
INSERT INTO personnes (prenom,nom,age,pays) VALUES('John','Doe',14,'Espagne');
INSERT INTO personnes (prenom,nom,age,pays) VALUES('John','Doe',18,'France');
INSERT INTO personnes (prenom,nom,age,pays) VALUES('Jo','Dalton',19,'USA');
INSERT INTO personnes (prenom,nom,age,pays) VALUES('Jo','Dalton',22,'USA');

-- Suppression d'une contrainte CHECK
ALTER TABLE personnes DROP CONSTRAINT chk_age_pays; 

INSERT INTO personnes (prenom,nom,age,pays) VALUES('John','Profit',19,'USA');

-- ON DELETE et ON UPDATE 
CREATE TABLE marques_cascade (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	date_creation DATE 
);

CREATE TABLE articles_cascade (
	id INT PRIMARY KEY AUTO_INCREMENT,
	description VARCHAR(150) NOT NULL,
	prix DECIMAL(6,2) NOT NULL DEFAULT 10.0,
	marque INT,
	
	CONSTRAINT fk_article_marque_cascade
	FOREIGN KEY (marque)
	REFERENCES marques_cascade(id)
);

INSERT INTO marques_cascade(nom,date_creation)  VALUES
('Marque A','1975-06-15'),
('Marque B','1995-01-05'),
('Marque C','1865-12-11');

INSERT INTO articles_cascade(description,prix,marque) VALUES 
('souris',15.0,3),
('souris gamer',45.0,3),
('cable HDMI 5M',9.0,1),
('TV HD',380.0,1),
('TV 4K',600.0,1),
('Clavier AZERTY',30.0,2);

-- La contrainte de clé étrangère empèche de supprimer une marque si elle associé a un ou plusieurs articles 
-- DELETE FROM marques_cascade WHERE id=2; -- -> ERREUR 

-- Il faut d'abord supprimer l'article id 6 qui est associé à la marque 2
DELETE FROM articles_cascade WHERE id=6;
-- Ensuite, on peut supprimer la marque id=2
DELETE FROM marques_cascade WHERE id=2;

-- ON DELETE CASCADE
-- On supprime la contrainte de clé étrangère fk_article_marque_cascade
ALTER TABLE articles_cascade DROP CONSTRAINT fk_article_marque_cascade ;
-- Si on ajoute ON DELETE CASCADE, lors de la création de la contrainte de clé étrangère
-- la supression d'une marque va automatiquement se propager au article associé avec cette marque
ALTER TABLE articles_cascade ADD CONSTRAINT fk_article_marque_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON DELETE CASCADE; -- Propagation de la supression

-- On supprime la marque id=1 
-- la suppression va être propagée aux articles id=3, id=4 et id=5
DELETE FROM marques_cascade WHERE id=1;

-- ON DELETE SET NULL
ALTER TABLE articles_cascade DROP CONSTRAINT fk_article_marque_cascade ;

-- Si on ajoute ON DELETE NULL, lors de la création de la contrainte de clé étrangère
-- lors de la supression d'une marque, la clé étrangère des articles associés auront pour valeur NULL
ALTER TABLE articles_cascade ADD CONSTRAINT fk_article_marque_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON DELETE SET NULL;

-- On supprime la marque id=3
-- la colonne marque des articles id=1 et id=3 est affectée à NULL
DELETE FROM marques_cascade WHERE id=3;

-- ON UPDATE CASCADE
INSERT INTO marques_cascade(nom,date_creation)  VALUES
('Marque A','1975-06-15'),
('Marque B','1995-01-05'),
('Marque C','1865-12-11');

INSERT INTO articles_cascade(description,prix,marque) VALUES 
('souris',15.0,6),
('souris gamer',45.0,6),
('cable HDMI 5M',9.0,4),
('TV HD',380.0,4),
('TV 4K',600.0,4),
('Clavier AZERTY',30.0,5);

ALTER TABLE articles_cascade DROP CONSTRAINT fk_article_marque_cascade ;

-- Avec On UPDATE CASCADE la modification de la clef primaire de marques va se propager à la 
-- colonne marque des articles associés à cette marque 
ALTER TABLE articles_cascade ADD CONSTRAINT fk_article_marque_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON UPDATE CASCADE ;

UPDATE marques_cascade SET id=7 WHERE id=4;

-- ON UPDATE SET NULL
ALTER TABLE articles_cascade DROP CONSTRAINT fk_article_marque_cascade ;
-- Avec On UPDATE NULL lorsque la clef primaire de marques est modifées
-- la colonne marque des articles associés est affecté à NULL
ALTER TABLE articles_cascade ADD CONSTRAINT fk_article_marque_cascade
FOREIGN KEY (marque)
REFERENCES marques_cascade(id) ON UPDATE SET NULL ;

UPDATE marques_cascade SET id=8 WHERE id=6;