USE exemple;

-- Ajouter des données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO marques VALUES (1,'Marque A','1975-06-15');

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO marques(nom_marque,date_creation) VALUES('Marque B','1980-03-01');
INSERT INTO marques(nom_marque) VALUES('Marque C');

-- On n'a pas définie nom_marque -> c'est la valeur par défaut qui sera utilisé
INSERT INTO marques(date_creation) VALUES('1904-01-20');

-- On n'a pas définie prix -> c'est la valeur par défaut qui sera utilisé
INSERT INTO articles(reference,nom_article,marque)
VALUES (10,'TV 4K',1);

-- Erreur ->  le prix ne pas être NULL (colonne NOT NULL)
-- INSERT INTO articles(reference,nom_article,prix,marque)
-- VALUES (100,'TV HD',NULL,1);

INSERT INTO articles(reference,nom_article,prix,marque)
VALUES (100,'TV HD',380.0,1);

-- Erreur -> La marque avec l'id 20 n'existe pas, la contrainte d'intégritée référentielle (fk_articles_marques)
-- , n'est pas respecté

-- INSERT INTO articles(reference,nom_article,prix,marque)
-- VALUES (200,'Clavier AZERY',30.0,20);

INSERT INTO articles(reference,nom_article,prix,marque)
VALUES (200,'Clavier AZERY',30.0,2);

-- Insérer Plusieurs lignes
INSERT INTO articles(reference,nom_article,prix,marque) VALUES 
(400,'souris',15.0,3),
(402,'souris gamer',45.0,3),
(500,'cable HDMI 5M',9.0,1);

-- Insérer des données dans une relation n-n
INSERT INTO fournisseurs (nom) VALUES 
('fournisseur 1'),
('fournisseur 2');

-- On fait le lien 
INSERT INTO fournisseur_article (id_fournisseur,id_articles) VALUES 
(1,100),
(1,200),
(1,500),
(2,400),
(2,402);


-- Supprimer des données => DELETE, TRUNCATE
INSERT INTO articles(reference,nom_article,prix,marque) VALUES 
(1000,'souris',1500.0,3),
(1001,'souris gamer',4500.0,3),
(1002,'cable HDMI 5M',9000.0,1);

-- Supression de la ligne qui a pour reference 1000 dans la table articles
DELETE FROM articles WHERE reference=1000;

-- Supprimer toutes les lignes de la table articles qui ont un prix > 2200
DELETE FROM articles WHERE prix >2000;

-- Supprimer toutes les lignes de la table fournisseurs_articles (il n'y a pas de condition)
DELETE FROM fournisseur_article;

-- Valeur auto_increment et la suppression
-- DELETE => ne ré-initialise pas la colonne AUTO_INCREMENT
DELETE FROM fournisseurs ;

INSERT INTO fournisseurs (nom) VALUES 
('fournisseur A'), -- id=3
('fournisseur B'); -- id=4

-- TRUNCATE => réinitialise l'auto incrément
SET FOREIGN_KEY_CHECKS =0;	-- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table articles  => remet la colonne AUTO_INCREMENT à 0
TRUNCATE TABLE fournisseurs; 
SET FOREIGN_KEY_CHECKS =1; -- réactiver la vérification des clés étrangères

INSERT INTO fournisseurs (nom) VALUES 
('fournisseur A'), -- id=1
('fournisseur B'); -- id=2


-- Les contraintes de clé étrangère
-- DELETE FROM marques WHERE id=1;
-- erreur -> on ne peut pas suprimer la marque avec l'id 1
-- tant qu'il y a des articles qui référence cette marque

-- On supprime tous les articles qui ont pour marque 1
DELETE FROM articles WHERE marque =1;

-- comme il n'y a plus d'article qui ont la marque 1, on peut supprimer la marque 1  
DELETE FROM marques WHERE id=1;

-- Modification des données => UPDATE
-- Modification le nom de l'article qui a pour reference 200
UPDATE articles SET nom_article ='Clavier QWERTY' WHERE reference=200;

-- Modification du nom et du prix de l'article qui a pour reference 200
UPDATE articles SET nom_article ='Clavier AZERTY', prix=45.0 WHERE reference=200;

-- Modifier tous les prix inférieur à 20.0 passe à 5.00 euros
UPDATE articles SET prix =5.0 WHERE prix<20.0;

-- Pas de condition -> tous les prix sont augmentés de 10%
UPDATE articles SET prix= prix*1.10;

-- Exercice: manipulation de données
USE pizzeria;

-- ajouter des ingrédients
INSERT INTO ingredients (nom) VALUES 
('mozarella'),
('champginon'),
('jambon'),
('anchois'),
('tomate');

-- ajouter des pizzas
INSERT INTO pizzas(nom, base,prix) VALUES 
('margarita','rouge',11.0),
('forestière','rouge',12.0),
('napolitaine',1,15.0);

-- associer les ingrédients aux pizzas
INSERT INTO pizzas_ingredients (numero_ingredient, numero_pizza) VALUES 
(5,1), -- margarita
(1,1),
(5,2), -- forestière
(2,2),
(3,2),
(5,3), -- napolitaine
(4,3);

-- modifier le prix de la Pizza napolitaine  de 15 à 13.5
UPDATE pizzas SET prix=13.5 WHERE numero_pizza=3; 

-- Augmenter tous les prix des pizzas de 20%
UPDATE pizzas SET prix=prix*1.2;

-- Supprimer la pizza Pizza forestière (numero_pizza=2)
-- On ne peut pas supprimer dirrectement la pizza à cause de la contrainte de clé étrangère 

-- Il faut supprimer les lignes concernant la pizza numero_pizza=2 dans la table 
DELETE FROM pizzas_ingredients WHERE numero_pizza =2;

-- et supprimer la pizza qui a pour numero_pizza 2
DELETE FROM pizzas WHERE numero_pizza =2;
