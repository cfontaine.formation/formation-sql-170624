-- Commentaire fin de ligne
# Commentaire fin de ligne

/* 
 * Commentaire
 * sur 
 * plusieurs
 * lignes
 */

-- Création de la base de données exemple
CREATE DATABASE exemple;

-- Supprimer un base de donnée
-- DROP DATABASE exemple;

-- Sélectionner la base de données courante
USE exemple;

-- Afficher l'ensemble des bases de données 
SHOW DATABASES;

-- Créer une table article
CREATE TABLE article(
	reference INT,
	description VARCHAR(200),
	prix DECIMAL(6,2)
);

-- Lister les tables de la base de données
SHOW tables;

-- Afficher la description de la table (mysql/mariadb)
DESCRIBE article;

-- Supprimer la table
DROP TABLE article;

-- Exercice: créer la table vols
CREATE TABLE vols(
	numero_vol VARCHAR(6),
	heure_depart DATETIME,
	ville_depart VARCHAR(60),
	heure_arrivee DATETIME,
	ville_arrivee VARCHAR(60)
);

-- Modifier une table -> ALTER TABLE

-- Modifier le nom d'une table (mysql/mariadb/IBM DB2)
RENAME TABLE article TO articles;
-- avec Sql Server : EXEC sp_rename 'article','articles';
-- avec PostgreSQL : ALTER TABLE article' RENAME TO articles;
-- avec Oracle: RENAME article TO articles;

-- Ajouter une colonne
ALTER TABLE articles ADD nom VARCHAR(20);

-- Supression d'une colonne
ALTER TABLE articles DROP description;

-- Modifier le type d'une colonne VARCHAR(20) -> VARCHAR(60)
ALTER TABLE articles MODIFY nom VARCHAR(60);

-- Modifier le nom d'une colonne (nom -> nom_article)
ALTER TABLE articles CHANGE nom nom_article VARCHAR(60);

-- Interdire les valeurs NULL sur une colonne -> NOT NULL

-- à la création de la table
-- CREATE TABLE articles(
-- 	reference INT,
-- 	nom_article VARCHAR(60),
-- 	prix DECIMAL(6,2) NOT NULL
-- );

-- Enumération -> ENUM
ALTER TABLE articles ADD emballage ENUM('CARTON','PLASTIQUE','PAPIER','SANS') NOT NULL DEFAULT 'SANS'  ; 

-- Ajouter une NOT NULL sur colonne existante
ALTER TABLE articles MODIFY prix DECIMAL(6,2) NOT NULL;

-- Valeur par défaut DEFAULT 

-- à la création de la table
-- CREATE TABLE articles(
-- 	reference INT,
-- 	nom_article VARCHAR(60) DEFAULT 'Inconue',
-- 	prix DECIMAL(6,2) NOT NULL DEFAULT 1.0
-- );

-- Modifier une colonne pour ajouter une valeur par défaut
ALTER TABLE articles MODIFY prix DECIMAL(6,2) NOT NULL DEFAULT 1.0;
ALTER TABLE articles MODIFY nom_article VARCHAR(60) DEFAULT 'Inconue';

-- Contrainte d’unicité -> UNIQUE
-- à la création de la table
CREATE TABLE stagiaires(
	id INT,
	prenom VARCHAR(60),
	nom VARCHAR(60),
	email VARCHAR(150) NOT NULL UNIQUE
);

-- Ajouter une containte nommé -> un_email_stagiaire
-- ALTER TABLE stagiaires ADD CONSTRAINT un_email_stagiaire UNIQUE(email);

-- Supprimer une containte nommé -> un_email_stagiaire
-- ALTER TABLE stagiaires DROP CONSTRAINT un_email_stagiaire;

-- si les contraintes ne sont pas nommées :
-- on peut utiliser le nom de la colonne pour supprimer la contrainte unique
ALTER TABLE stagiaires DROP CONSTRAINT email;


-- Clé primaire
-- Une clé primaire est la donnée qui permet d'identifier de manière unique une ligne dans une table

-- à la création de la table
-- CREATE TABLE articles(
-- 	reference INT PRIMARY KEY,
-- 	nom_article VARCHAR(60),
-- 	prix DECIMAL(6,2) NOT NULL DEFAULT 1.0
-- );

-- Modifier la table articles pour que la colonne reference soit la clé primaire de la table
ALTER TABLE articles ADD CONSTRAINT pk_articles PRIMARY KEY(reference);

-- pour une clé primaire le nom de la contrainte n'est pas obligatoire, on pourrait l'écrire aussi :
-- ALTER TABLE articles ADD CONSTRAINT PRIMARY KEY(reference);

-- Modifier la table stagiaires pour que la colonne id soit la colonne id de la table
ALTER TABLE stagiaires ADD CONSTRAINT pk_stagiaires PRIMARY KEY(id); 

-- on peut utiliser directement PRIMARY KEYpout supprimer la clé primaire
ALTER TABLE stagiaires DROP CONSTRAINT PRIMARY KEY;

-- AUTO_INCREMENT 
-- -> la valeur de la clé primaire sera incrémentée automatiquement à chaque ajout d’une ligne dans la table
-- uniquement pour des clés de type entier

-- à la création de la table
-- CREATE TABLE stagiaires(
-- 	id INT PRIMARY KEY AUTO_INCREMENT,
-- 	prenom VARCHAR(40),
-- 	nom VARCHAR(40),
-- 	email VARCHAR(150) UNIQUE
-- );

-- Modifier la colonne référence pour ajouter AUTO_INCREMENT sur la clé primaire
ALTER TABLE stagiaires MODIFY id INT AUTO_INCREMENT;
ALTER TABLE stagiaires MODIFY id INT;
-- Exercice table pilotes
CREATE TABLE pilotes (
	numero_pilote INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	nombre_heure_vol INT NOT NULL
);

-- Exercice table vols
-- Modifier la table vols pour que la colonne numero_vol devienne la clé primaire de la table
ALTER TABLE vols ADD CONSTRAINT pk_vols PRIMARY KEY(numero_vol);

-- Ajouter NOT NULL sur les colonnes ville_depart et ville_arrive
ALTER TABLE vols MODIFY ville_depart VARCHAR(60) NOT NULL;
ALTER TABLE vols MODIFY ville_arrivee VARCHAR(60) NOT NULL;

CREATE TABLE avions
(
	numero_avion INT PRIMARY KEY AUTO_INCREMENT,
	modele VARCHAR(50) NOT NULL,
	capacite SMALLINT NOT NULL
);

-- Relation entre table

-- Relation 1,n
CREATE TABLE marques
(	id INT PRIMARY KEY AUTO_INCREMENT ,
	nom_marque VARCHAR(60),
	date_creation DATE
);

-- On ajoute une colonne qui sera la clé étrangère ( du coté 1 de la relation)
ALTER TABLE articles ADD marque INT;

-- Ajout d'une contrainte de clé étrangère 
-- clé étrangère -> colonne marque de articles
-- elle fait référence à la clé primaire id de la table marques
ALTER TABLE articles ADD CONSTRAINT fk_articles_marques
FOREIGN KEY (marque)
REFERENCES marques(id);

-- à la création de la table
-- L'ordre de création des tables est important
-- marque doit être créé avant articles
-- CREATE TABLE articles(
-- 	reference INT PRIMARY KEY,
-- 	nom_article VARCHAR(60),
-- 	prix DECIMAL(6,2) NOT NULL DEFAULT 1.0
-- 	marque INT,
-- 	CONSTRAINT  fk_articles_marques
-- 	FOREIGN KEY marque 
-- 	REFERENCES marques(id)
-- );

ALTER TABLE vols ADD pilote INT;

ALTER TABLE vols ADD CONSTRAINT fk_vol_pilote
FOREIGN KEY (pilote)
REFERENCES pilotes(numero_pilote);

ALTER TABLE vols ADD avion INT;

ALTER TABLE vols ADD CONSTRAINT fk_vol_avion
FOREIGN KEY (avion)
REFERENCES avions(numero_avion);

-- Relations n-n
CREATE TABLE fournisseurs(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(80) NOT NULL 
)

-- table de jonction
CREATE TABLE fournisseur_article(
	id_fournisseur INT,
	id_articles INT
);

-- Clé étrangères entre fournisseurs et la table de jointure
ALTER TABLE fournisseur_article ADD CONSTRAINT fk_fournisseurs_articles
FOREIGN KEY (id_fournisseur)
REFERENCES fournisseurs (id);

-- Clé étrangères entre articles et la table de jointure
ALTER TABLE fournisseur_article ADD CONSTRAINT fk_articles_fournisseurs
FOREIGN KEY (id_articles)
REFERENCES articles(reference);

-- Clé primaire composée
ALTER TABLE fournisseur_article ADD CONSTRAINT PRIMARY KEY(id_fournisseur,id_articles);

-- Création de la table de jointure avec les contraintes de clé étrangère et la clé primaire
-- CREATE TABLE fournisseurs_articles
-- (
-- 	id_article INT,
-- 	id_fournisseur INT,
-- 	
-- 	CONSTRAINT fk_fournisseurs_articles
-- 	FOREIGN KEY (id_fournisseur)
-- 	REFERENCES fournisseurs(id),
-- 	
-- 	CONSTRAINT fk_articles_fournisseurs
-- 	FOREIGN KEY (id_article)
-- 	REFERENCES articles(reference),
-- 	
-- 	-- clé primaire composée id_article et id_fournisseur
-- 	CONSTRAINT pk_fournisseurs_articles
-- 	PRIMARY KEY(id_article,id_fournisseur)
-- );
