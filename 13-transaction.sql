USE exemple;

CREATE TABLE compte_bancaire(
	id INT PRIMARY KEY AUTO_INCREMENT,
	solde DECIMAL(10,3),
	iban VARCHAR(20),
	titulaire VARCHAR(50)
);

INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES
(1000.000,'FR-105-0000-01','John Doe'),
(5000.000,'FR-105-0000-02','Jane Doe'),
(500.000,'FR-105-0000-03','Alan Smithee'),
(400.000,'FR-105-0000-04','Yves Roulo'),
(4000.000,'FR-105-0000-05','Jo Dalton');

-- Atomicité
-- désactivation de l'autocommit
SET autocommit=0;

START TRANSACTION; -- Commencer une TRANSACTION
-- transfert d'argent entre 2 comptes
UPDATE compte_bancaire SET solde=solde -200.0 WHERE id=1;
UPDATE compte_bancaire SET solde=solde +200.0 WHERE id=3;
COMMIT; -- valider la TRANSACTION

SELECT * FROM compte_bancaire ;

START TRANSACTION; -- Commencer une TRANSACTION
-- transfert d'argent entre 2 comptes
UPDATE compte_bancaire SET solde=solde -50.0 WHERE id=2;
ROLLBACK; -- annuler a TRANSACTION

-- Les points de sauvegarde
START TRANSACTION;
UPDATE compte_bancaire SET solde=solde-300.0 WHERE id=3;
UPDATE compte_bancaire SET solde=solde+300.0 WHERE id=4;
SAVEPOINT transfert1; -- Création d'un point de sauvegarde
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES 
(100.0,"FR-105-0000-10",'Antoine Berretto');
ROLLBACK  TO transfert1 ; -- Annulation du INSERT et retour au point de sauvegarde transfert1
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES 
(1500.0,"FR-105-0000-10",'Antoine Beretto');
SAVEPOINT creation_compte_beretto;
UPDATE compte_bancaire SET solde=solde-500.0 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+500.0 WHERE id=2;

-- Libérer les points de sauvegardes
RELEASE SAVEPOINT creation_compte_beretto;
RELEASE SAVEPOINT transfert1;
COMMIT; -- Rendre la transaction persistente
SELECT * FROM compte_bancaire;

-- pour gérer la transaction avec dbeaver
-- Menu -> base de données -> Mode transactionel -> commit manuel
START TRANSACTION; -- Commencer une TRANSACTION
-- transfert d'argent entre 2 comptes
UPDATE compte_bancaire SET solde=solde -200.0 WHERE id=1;
UPDATE compte_bancaire SET solde=solde +200.0 WHERE id=3;
-- valider la transaction -> Appliquer (commit)
-- anuller la transaction ->Retour arrière (rollback)
-- Menu -> base de données -> Mode transactionel -> auto-commit

-- Création d'un utilisateur
-- créer un utilisateur user1 avec le mot de passe 'dawan' en local sur la machine
CREATE USER 'user1'@'localhost' IDENTIFIED BY 'dawan';

-- GRANT droit ON base_de_donnees.table TO utilisateur WITH GRANT OPTION;
-- droit ALL -> tout
--		 CREATE, ALTER, DROP INSERT, UPDATE, DELETE,REFERENCES, FLUSH , RELOAD  ...
-- Pour toutes les tables d'une base de données base_de_donnees.*
GRANT ALL ON exemple.compte_bancaire TO 'user1'@'localhost';

FLUSH PRIVILEGES;

-- voir les droits d'un utilisateur
SHOW GRANTS FOR 'user1'@'localhost';

-- SUPPRIMER Un UTILISATEUR
DROP USER 'user1'@'localhost'; 

-- activation de l'autocommit
SET Autocommit=1;

USE bibliotheque;

-- INDEX -> permet d'accélérer l’exécution d’une requête SQL qui lit des données
SELECT livres.id,titre,annee,genres.nom FROM livres 
INNER JOIN genres ON genre=genres.id
WHERE annee BETWEEN 1964 AND 1984;

-- ajouter un index sur la colonne annee de livres
CREATE INDEX ind_livres_annee ON livres(annee);

-- plan d'execution ->  permet de savoir de quelle manière le SGBD va exécuter la requête( utilisation des index ...)
-- la requête ne renverra pas les résultats du SELECT mais plutôt une analyse de cette requête
EXPLAIN SELECT id,titre,annee,genre FROM livres 
WHERE annee BETWEEN 1964 AND 1984;

EXPLAIN SELECT livres.id,titre,annee,genres.nom FROM livres 
INNER JOIN genres ON genre=genres.id
WHERE annee BETWEEN 1964 AND 1984;

-- suprimer l'index
DROP INDEX ind_livres_annee ON livres;

-- Exporter les résultats vers un fichier csv
SELECT * FROM livres INTO OUTFILE 'c:/Dawan/livres3.csv'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n';
