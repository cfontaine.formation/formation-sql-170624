-- Base de données: Elevage
-- (MySQL/MariaDB)
-- Juin 2024
-- ------------------------
DROP DATABASE IF EXISTS elevage;
CREATE DATABASE IF NOT EXISTS elevage;

USE elevage;

-- Table espece
CREATE TABLE espece (
  id INT PRIMARY KEY  AUTO_INCREMENT,
  nom_courant VARCHAR(40) NOT NULL,
  nom_latin VARCHAR(40) NOT NULL,
  description TEXT,
  UNIQUE KEY nom_latin (nom_latin)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO espece  VALUES(1, 'Chien', 'Canis canis', 'Bestiole à quatre pattes qui aime les caresses et tire souvent la langue');
INSERT INTO espece  VALUES(2, 'Chat', 'Felis silvestris', 'Bestiole à quatre pattes qui saute très haut et grimpe aux arbres');
INSERT INTO espece  VALUES(3, 'Tortue d''Hermann', 'Testudo hermanni', 'Bestiole avec une carapace très dure');
INSERT INTO espece  VALUES(4, 'Perroquet amazone', 'Alipiopsitta xanthops', 'Joli oiseau parleur vert et jaune');

-- Table race
CREATE TABLE race (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(40) NOT NULL,
  espece_id INT NOT NULL,
  description TEXT DEFAULT NULL,
  CONSTRAINT fk_race_espece_id FOREIGN KEY (espece_id) REFERENCES espece (id)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO race  VALUES(1, 'Berger allemand', 1, 'Chien sportif et élégant au pelage dense, noir-marron-fauve, noir ou gris.');
INSERT INTO race  VALUES(2, 'Berger blanc suisse', 1, 'Petit chien au corps compact, avec des pattes courtes mais bien proportionnées et au pelage tricolore ou bicolore.');
INSERT INTO race  VALUES(3, 'Boxer', 1, 'Chien de taille moyenne, au poil ras de couleur fauve ou bringé avec quelques marques blanches.');
INSERT INTO race  VALUES(4, 'Bleu russe', 2, 'Chat aux yeux verts et à la robe épaisse et argentée.');
INSERT INTO race  VALUES(5, 'Maine coon', 2, 'Chat de grande taille, à poils mi-longs.');
INSERT INTO race  VALUES(6, 'Singapura', 2, 'Chat de petite taille aux grands yeux en amandes.');
INSERT INTO race  VALUES(7, 'Sphynx', 2, 'Chat sans poils.');

-- Table animal
CREATE TABLE animal (
  id INT PRIMARY KEY AUTO_INCREMENT,
  sexe CHAR(1),
  date_naissance datetime NOT NULL,
  nom varCHAR(30),
  commentaires TEXT,
  espece_id INT,
  race_id INT,
  mere_id INT,
  pere_id INT
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO animal VALUES(1, 'M', '2010-04-05 13:43:00.000', 'Rox', 'Mordille beaucoup', 1, 1, 18, 22);
INSERT INTO animal VALUES(2, NULL, '2010-03-24 02:23:00.000', 'Roucky', NULL, 2, NULL, 40, 30);
INSERT INTO animal VALUES(3, 'F', '2010-09-13 15:02:00.000', 'Schtroumpfette', NULL, 2, 4, 41, 31);
INSERT INTO animal VALUES(4, 'F', '2009-08-03 05:12:00.000', NULL, NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(5, NULL, '2010-10-03 16:44:00.000', 'Choupi', 'Né sans oreille gauche', 2, NULL, NULL, NULL);
INSERT INTO animal VALUES(6, 'F', '2009-06-13 08:17:00.000', 'Bobosse', 'Carapace bizarre', 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(7, 'F', '2008-12-06 05:18:00.000', 'Caroline', NULL, 1, 2, NULL, NULL);
INSERT INTO animal VALUES(8, 'M', '2008-09-11 15:38:00.000', 'Bagherra', NULL, 2, 5, NULL, NULL);
INSERT INTO animal VALUES(9, NULL, '2010-08-23 05:18:00.000', NULL, NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(10, 'M', '2010-07-21 15:41:00.000', 'Bobo', NULL, 1, NULL, 7, 21);
INSERT INTO animal VALUES(11, 'F', '2008-02-20 15:45:00.000', 'Canaille', NULL, 1, NULL, NULL, NULL);
INSERT INTO animal VALUES(12, 'F', '2009-05-26 08:54:00.000', 'Cali', NULL, 1, 2, NULL, NULL);
INSERT INTO animal VALUES(13, 'F', '2007-04-24 12:54:00.000', 'Rouquine', NULL, 1, 1, NULL, NULL);
INSERT INTO animal VALUES(14, 'F', '2009-05-26 08:56:00.000', 'Fila', NULL, 1, 2, NULL, NULL);
INSERT INTO animal VALUES(15, 'F', '2008-02-20 15:47:00.000', 'Anya', NULL, 1, NULL, NULL, NULL);
INSERT INTO animal VALUES(16, 'F', '2009-05-26 08:50:00.000', 'Louya', NULL, 1, NULL, NULL, NULL);
INSERT INTO animal VALUES(17, 'F', '2008-03-10 13:45:00.000', 'Welva', NULL, 1, 3, NULL, NULL);
INSERT INTO animal VALUES(18, 'F', '2007-04-24 12:59:00.000', 'Zira', NULL, 1, 1, NULL, NULL);
INSERT INTO animal VALUES(19, 'F', '2009-05-26 09:02:00.000', 'Java', NULL, 1, 2, NULL, NULL);
INSERT INTO animal VALUES(20, 'M', '2007-04-24 12:45:00.000', 'Balou', NULL, 1, 1, NULL, NULL);
INSERT INTO animal VALUES(21, 'F', '2008-03-10 13:43:00.000', 'Pataude', NULL, 1, 3, NULL, NULL);
INSERT INTO animal VALUES(22, 'M', '2007-04-24 12:42:00.000', 'Bouli', NULL, 1, 1, NULL, NULL);
INSERT INTO animal VALUES(24, 'M', '2007-04-12 05:23:00.000', 'Cartouche', NULL, 1, NULL, NULL, NULL);
INSERT INTO animal VALUES(25, 'M', '2006-05-14 15:50:00.000', 'Zambo', NULL, 1, 1, NULL, NULL);
INSERT INTO animal VALUES(26, 'M', '2006-05-14 15:48:00.000', 'Samba', NULL, 1, 1, NULL, NULL);
INSERT INTO animal VALUES(27, 'M', '2008-03-10 13:40:00.000', 'Moka', NULL, 1, 3, NULL, NULL);
INSERT INTO animal VALUES(28, 'M', '2006-05-14 15:40:00.000', 'Pilou', NULL, 1, 1, NULL, NULL);
INSERT INTO animal VALUES(29, 'M', '2009-05-14 06:30:00.000', 'Fiero', NULL, 2, 6, NULL, NULL);
INSERT INTO animal VALUES(30, 'M', '2007-03-12 12:05:00.000', 'Zonko', NULL, 2, 5, NULL, NULL);
INSERT INTO animal VALUES(31, 'M', '2008-02-20 15:45:00.000', 'Filou', NULL, 2, 4, NULL, NULL);
INSERT INTO animal VALUES(32, 'M', '2007-03-12 12:07:00.000', 'Farceur', NULL, 2, 5, NULL, NULL);
INSERT INTO animal VALUES(33, 'M', '2006-05-19 16:17:00.000', 'Caribou', NULL, 2, 4, NULL, NULL);
INSERT INTO animal VALUES(34, 'M', '2008-04-20 03:22:00.000', 'Capou', NULL, 2, 5, NULL, NULL);
INSERT INTO animal VALUES(35, 'M', '2006-05-19 16:56:00.000', 'Raccou', 'Pas de queue depuis la naissance', 2, 4, NULL, NULL);
INSERT INTO animal VALUES(36, 'M', '2009-05-14 06:42:00.000', 'Boucan', NULL, 2, 6, NULL, NULL);
INSERT INTO animal VALUES(37, 'F', '2006-05-19 16:06:00.000', 'Callune', NULL, 2, 4, NULL, NULL);
INSERT INTO animal VALUES(38, 'F', '2009-05-14 06:45:00.000', 'Boule', NULL, 2, 6, NULL, NULL);
INSERT INTO animal VALUES(39, 'F', '2008-04-20 03:26:00.000', 'Zara', NULL, 2, 5, NULL, NULL);
INSERT INTO animal VALUES(40, 'F', '2007-03-12 12:00:00.000', 'Milla', NULL, 2, 5, NULL, NULL);
INSERT INTO animal VALUES(41, 'F', '2006-05-19 15:59:00.000', 'Feta', NULL, 2, 4, NULL, NULL);
INSERT INTO animal VALUES(42, 'F', '2008-04-20 03:20:00.000', 'Bilba', 'Sourde de l''oreille droite à 80%', 2, 5, NULL, NULL);
INSERT INTO animal VALUES(43, 'F', '2007-03-12 11:54:00.000', 'Cracotte', NULL, 2, 5, NULL, NULL);
INSERT INTO animal VALUES(44, 'F', '2006-05-19 16:16:00.000', 'Cawette', NULL, 2, 4, NULL, NULL);
INSERT INTO animal VALUES(45, 'F', '2007-04-01 18:17:00.000', 'Nikki', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(46, 'F', '2009-03-24 08:23:00.000', 'Tortilla', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(47, 'F', '2009-03-26 01:24:00.000', 'Scroupy', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(48, 'F', '2006-03-15 14:56:00.000', 'Lulla', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(49, 'F', '2008-03-15 12:02:00.000', 'Dana', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(50, 'F', '2009-05-25 19:57:00.000', 'Cheli', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(51, 'F', '2007-04-01 03:54:00.000', 'Chicaca', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(52, 'F', '2006-03-15 14:26:00.000', 'Redbul', 'Insomniaque', 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(53, 'M', '2007-04-02 01:45:00.000', 'Spoutnik', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(54, 'M', '2008-03-16 08:20:00.000', 'Bubulle', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(55, 'M', '2008-03-15 18:45:00.000', 'Relou', 'Surpoids', 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(56, 'M', '2009-05-25 18:54:00.000', 'Bulbizard', NULL, 3, NULL, NULL, NULL);
INSERT INTO animal VALUES(57, 'M', '2007-03-04 19:36:00.000', 'Safran', NULL, 4, NULL, NULL, NULL);
INSERT INTO animal VALUES(58, 'M', '2008-02-20 02:50:00.000', 'Gingko', NULL, 4, NULL, NULL, NULL);
INSERT INTO animal VALUES(59, 'M', '2009-03-26 08:28:00.000', 'Bavard', NULL, 4, NULL, NULL, NULL);
INSERT INTO animal VALUES(60, 'F', '2009-03-26 07:55:00.000', 'Parlotte', NULL, 4, NULL, NULL, NULL);

ALTER TABLE animal ADD CONSTRAINT fk_espece_id FOREIGN KEY (espece_id) REFERENCES espece (id);
ALTER TABLE animal ADD CONSTRAINT fk_mere_id FOREIGN KEY (mere_id) REFERENCES animal (id);
ALTER TABLE animal ADD CONSTRAINT fk_pere_id FOREIGN KEY (pere_id) REFERENCES animal (id);
ALTER TABLE animal ADD CONSTRAINT fk_race_id FOREIGN KEY (race_id) REFERENCES race (id);