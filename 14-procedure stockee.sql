USE bibliotheque;

-- 1. Déclaration d'un procedure stockée
 DELIMITER $ -- Changer le délimiteur de fin de ligne  ; -> $
 CREATE PROCEDURE auteur_vivant()
 BEGIN
 	SELECT prenom,nom,naissance FROM auteurs WHERE deces IS NULL; 
 END $
 DELIMITER ;  -- Changer le délimiteur de fin de ligne   $ -> ;


-- 2. Variables locales / globales 
 DELIMITER $
 CREATE PROCEDURE test_variable()
 BEGIN
	-- Variable locale -> Elle n'existe que dans la procédure stockée
	 
 	-- Déclarer une variable locale
	-- Si la variable n'est pas initialisée avec DEFAULT, elle aura pour valeur NULL
 	DECLARE v_local INT DEFAULT 42;
 	SELECT v_local;	-- Afficher le contenu de la variable v_local
 
 	-- Affecter une valeur à une variable locale -> SET
 	SET v_local=4;
 	SELECT v_local;	-- Afficher le contenu de la variable v_local
 
 	-- Affecter le résultat d'une requête  à une variable -> INTO
 	SELECT count(id) INTO v_local FROM auteurs;
 	SELECT v_local; -- Afficher le contenu de la variable v_local
 	
 	-- Variable global @ -> accessible partout
 	-- Il vaut mieux utiliser les paramètres plutôt qu'une variable globale dans une procédure stockée
 	SELECT @v_global;
 	-- Affecter le contenu de la variable locale  dans la variable globale
 	SET @v_global=v_local;
 END $
 DELIMITER ;

-- 3. Passage de paramètres
-- Un paramètre est composé de 3 parties :
--	- le sens: IN -> en entré, OUT -> en sortie ou en entré et en sortie -> INOUT
--  - le nom du paramètre
--  - le type du paramètre 

-- Procédure pour ajouter 2 entiers
 DELIMITER $
 -- 2 paramètres entiers en entrée : a et b
 -- un paramètre entier en sortie: somme
 CREATE PROCEDURE addition(IN a INT,IN b INT,OUT somme INT)
 BEGIN
	-- On ajoute les 2 paramètres d'entrées a et b et on affecte le résultat au paramètre de sortie 
 	SET somme = a + b;
 END $
 DELIMITER ;

 DELIMITER $
 CREATE PROCEDURE nb_livre_genre(IN nom_genre VARCHAR(50),OUT nb_livres INT)
 BEGIN
 	SELECT count(livres.id) INTO nb_livres FROM livres
 	INNER JOIN genres ON livres.genre=genres.id
 	WHERE genres.nom=nom_genre;
 END $
 DELIMITER ;

-- Exercice Procédure Stockée
-- Créer une procédure qui a pour nom: nb_ville_pays en paramètre, on a:
--  - en entrée un entier code_country
--  - en sortie un entier nb_ville_pays
-- La procédure pour le code_country passé en paramètre va retourner le nombre de ville
USE world;

DELIMITER $
CREATE PROCEDURE nb_ville_pays(IN code_country CHAR(3),OUT nb_ville_pays INT)
BEGIN
	SELECT count(id) INTO nb_ville_pays FROM city WHERE STRCMP(city.countrycode,code_country COLLATE utf8mb4_general_ci)=0;
END $
DELIMITER ;

USE bibliotheque;

-- 4. Instruction conditionnelle -> IF
 DELIMITER $
 CREATE PROCEDURE even(IN val INT,OUT str VARCHAR(15))
 BEGIN
 	IF val MOD 2 = 0 THEN
 		SET str='Pair';
 	ELSE
 		SET str='Impair';
 	END IF;
 END $
 DELIMITER ;

-- 5.  Instruction conditionnelle -> CASE
-- DELIMITER $
-- CREATE PROCEDURE genre_couleur(IN genre VARCHAR(50),OUT couleur VARCHAR(30))
-- BEGIN 
-- 	CASE genre 
-- 		WHEN 'Policier' THEN SET couleur='noir';
-- 		WHEN 'science-fiction' THEN SET couleur='orange';
-- 		WHEN 'horreur' THEN SET couleur='rouge';
-- 		ELSE 
-- 			SET couleur='blanc';
-- 	END CASE;
-- END $
-- DELIMITER ;

-- 6. Boucle LOOP
DELIMITER $
CREATE PROCEDURE somme_multi_loop(IN valeur INT,OUT resultat INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET resultat=0;
exit_loop: LOOP
		SET i=i+1;
		SET resultat=resultat+i;
		IF i>=valeur THEN # pour quitter la boucle
			LEAVE exit_loop;
		END IF;
	END LOOP;
END $
DELIMITER ;

-- 7. Boucle tant que -> while
DELIMITER $
CREATE PROCEDURE somme_multi_while(IN valeur INT,OUT resultat INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET resultat=0;
	WHILE i<valeur DO
		SET i=i+1;
		SET resultat=resultat+i;
	END WHILE;	
END $
DELIMITER ;

-- 8. Boucle jusqu'a -> repeat
DELIMITER $
CREATE PROCEDURE somme_multi_repeat(IN valeur INT,OUT resultat INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET resultat=0;
	REPEAT 
		SET i=i+1;
		SET resultat=resultat+i;
	UNTIL i>=valeur END REPEAT;

END $
DELIMITER ;


-- 9. Utilisation d'un curseur pour parcourir un résultat
DELIMITER $
CREATE PROCEDURE auteurs_livre(IN id_livre INT,OUT auteurs VARCHAR(500))
BEGIN 
	DECLARE done INT DEFAULT 0;
	DECLARE a VARCHAR(100);

	DECLARE c_auteur CURSOR FOR  SELECT CONCAT(prenom,' ',nom) FROM livre2auteur
		INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur
		WHERE livre2auteur.id_livre=id_livre;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=1;

	OPEN c_auteur; 
	SET auteurs='';
	WHILE done=0 DO
		FETCH c_auteur INTO a;
		IF done=0 THEN 
			SET auteurs=CONCAT_WS(', ',auteurs,a);
		END IF;
	END WHILE;
	CLOSE c_auteur;
END $
DELIMITER ;
