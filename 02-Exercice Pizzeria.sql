CREATE DATABASE pizzeria;

USE pizzeria;

CREATE TABLE ingredients(
	numero_ingredient INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(80) DEFAULT 'Inconnue'
);

CREATE TABLE pizzas(
	numero_pizza int PRIMARY KEY AUTO_INCREMENT,
 	nom VARCHAR(40) NOT NULL,
 	base ENUM('rouge','blanche','rose') NOT NULL,
 	prix DECIMAL(4,2) NOT NULL DEFAULT 12.0,
 	photo BLOB(64000)
);

-- table de jointure
CREATE TABLE pizzas_ingredients(
	numero_pizza INT,
	numero_ingredient INT,
	
	-- clé étrangère entre la table de jointure et ingrédients
	CONSTRAINT fk_pizzas_ingredients
	FOREIGN KEY(numero_pizza)
	REFERENCES pizzas(numero_pizza),
	
	-- clé étrangère entre la table de jointure et ingrédients
	CONSTRAINT fk_ingredients_pizzas
	FOREIGN KEY (numero_ingredient)
	REFERENCES ingredients(numero_ingredient),
	
	-- Création d'une clé primaire composée
	CONSTRAINT
	PRIMARY KEY (numero_pizza,numero_ingredient)
);

-- --------------------------------------------
-- Suite: création des autre tables

CREATE TABLE livreurs(
	numero_livreur INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL,
	telephone CHAR(10) NOT NULL
);

CREATE TABLE clients(
	numero_client INT AUTO_INCREMENT PRIMARY KEY ,
	nom VARCHAR(50) NOT NULL,
	adresse CHAR(255) NOT NULL
);

CREATE TABLE commandes(
	numero_commande INT PRIMARY KEY AUTO_INCREMENT,
	heure_commande DATETIME NOT NULL,
	heure_livraison DATETIME NOT NULL,
	livreur INT,
	client INT,
	
	CONSTRAINT fk_commandes_livreurs
	FOREIGN  KEY (livreur)
	REFERENCES livreurs(numero_livreur),
	
	CONSTRAINT fk_commandes_clients
	FOREIGN  KEY (client)
	REFERENCES clients(numero_client)
);

-- Table de jonction entre pizza et commande
CREATE TABLE pizzas_commandes(
	num_pizza INT,
	num_commande INT,
	quantite INT NOT NULL DEFAULT 1,
	
	CONSTRAINT fk_pizzas_commandes
	FOREIGN KEY (num_pizza)
	REFERENCES pizzas(numero_pizza),
	
	CONSTRAINT fk_commandes_pizzas
	FOREIGN KEY (num_commande)
	REFERENCES commandes(numero_commande),
	
	CONSTRAINT PRIMARY KEY (num_pizza,num_commande)
);
