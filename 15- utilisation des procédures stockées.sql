USE bibliotheque;
-- 1. Procédure stockée
-- Appel d'une procedure stockée auteur_vivant
CALL auteur_vivant();

-- Afficher la liste de procédure stockée
SHOW PROCEDURE STATUS ;

-- 2. Variable
-- Variable utilisateur / session -> variable globale: @nom_variable
-- Elle visible partout

-- Affecter une valeur à une variable globale 
SET @v_global='test variable globale';
-- Afficher le contenu d'une variable 
SELECT @v_global;

CALL test_variable ();
SELECT @v_global; -- Afficher le contenu de la variable globale
-- On ne peut pas accéder à une variable locale en dehors de la procédrue stockée où elle déclarée
-- SELECT v_local; -- Erreur

-- 3. Passage de paramètres
-- On définit les valeurs des paramètres d'entré 2 pour a et 3 pour b
-- On récupère le résultat qui se trouve dans le paramètre de sortie avec une variable globale
CALL addition(2,3,@v_global);
SELECT @v_global; -- Afficher le contenu de la variable global 

CALL nb_livre_genre('Drame',@v_global);
SELECT @v_global;

-- Exercice: procédure stockée
USE world;

CALL nb_ville_pays('ITA',@var_global);
SELECT @var_global ;

USE bibliotheque;

-- 4. Instruction conditionnelle -> IF
CALL even(4,@v_global);
SELECT @v_global;	-- Affiche pair

CALL even(9,@v_global);
SELECT @v_global;	-- Affiche impair

-- 5. Instruction conditionnelle -> CASE
CALL genre_couleur('Aventure',@v_global);
SELECT @v_global;

-- 6. Boucle LOOP
CALL somme_multi_loop(5,@var_global);
SELECT @var_global;

-- 7. Boucle WHILE
CALL somme_multi_while(5,@var_global);
SELECT @var_global;

-- 8. Boucle REPEAT
CALL somme_multi_repeat(5,@var_global);
SELECT @var_global;

-- 9. Utilisation d'un curseur pour parcourir un résultat
CALL auteurs_livre(136,@v_global);
SELECT @v_global;

-- SUPRIMER les procédure stockées
DROP PROCEDURE auteur_vivant ;
DROP PROCEDURE test_variable;
DROP PROCEDURE nb_livre_genre;