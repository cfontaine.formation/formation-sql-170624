USE exemple;

CREATE TABLE employees(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_personnel VARCHAR(255),
	salaire_mensuel double,
	temps_travail_semaine INT
);


CREATE TABLE clients(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_livraison VARCHAR(255),
	adresse_personnel VARCHAR(255)
);

INSERT INTO employees(prenom,nom, adresse_personnel,salaire_mensuel,temps_travail_semaine)
VALUES
('John','Doe','1,rue esquermoise Lille',1800,35),
('Jane','Doe','1,rue esquermoise Lille',2000,35),
('Jo','Dalton','46,rue des Cannoniers  Lille',2800,40),
('Alan','Smithee','32 Boulevard Vincent Gache Nantes',2300,37),
('Yves','Roulo','1,46,rue des Cannoniers  Lille',1400,20);

INSERT INTO Clients(prenom,nom,adresse_livraison , adresse_personnel)
VALUES
('John','Doe','1,rue esquermoise Lille','1,rue esquermoise Lille'),
('Pierre','Martin','23,rue esquermoise Lille','23,rue esquermoise Lille'),
('Bastien','Dupond','46,rue des Cannoniers  Lille','46,rue des Cannoniers  Lille');


-- UNION -> supression des doublons
SELECT prenom,nom,adresse_personnel FROM employees 
UNION
SELECT prenom,nom,adresse_personnel FROM clients ;

-- UNION ALL -> autorise les doublons
SELECT prenom,nom,adresse_personnel FROM employees 
UNION ALL
SELECT prenom,nom,adresse_personnel FROM clients ;

SELECT prenom,nom,adresse_personnel,'employe' AS identifiant FROM employees 
UNION
SELECT prenom,nom,adresse_personnel,'client' AS identifiant  FROM clients ;

-- INTERSECT: on obtient se qui est commun entre les 2 résultats
SELECT prenom,nom,adresse_personnel FROM employees 
INTERSECT 
SELECT prenom,nom,adresse_personnel FROM clients ;

-- EXCEPT
SELECT prenom,nom,adresse_personnel FROM employees 
EXCEPT 
SELECT prenom,nom,adresse_personnel FROM clients ;

-- Exercice : opérateur de jeu
-- Afficher la liste des pays et des villes ayant moins de 1000 habitant
USE world;
SELECT name,'pays' AS type,population FROM country WHERE population<1000
UNION
SELECT name,'ville' AS type,population FROM city WHERE population<1000;

-- Liste des pays où on parle français ou anglais
SELECT name, 'fr' AS langue FROM country 
INNER JOIN country_language ON country.code=country_language.country_code
WHERE LANGUAGE= 'french'
UNION
SELECT name, 'en' AS langue FROM country 
INNER JOIN country_language ON country.code=country_language.country_code
WHERE LANGUAGE= 'english';

-- On peut avoir le m^me résultat avec une jointure
SELECT name,LOWER(LEFT(LANGUAGE,2)) as langue FROM country
INNER JOIN  country_language ON country.code=country_language.country_code
WHERE LANGUAGE IN ('french','english');

